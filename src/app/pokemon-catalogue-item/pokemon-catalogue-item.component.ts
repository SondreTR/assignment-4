import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Pokemon} from "../models/pokemon.model";

@Component({
  selector: 'app-pokemon-catalogue-item',
  templateUrl: './pokemon-catalogue-item.component.html',
})
export class PokemonCatalogueItemComponent {
  @Input() pokemon: Pokemon | undefined; 
  @Output() clicked: EventEmitter<Pokemon> = new EventEmitter<Pokemon>();

  public onPokemonClicked(): void {
    this.clicked.emit(this.pokemon);
  }

  public capitalizedPokemonName(name: any) {
    return name.charAt(0).toUpperCase() + name.slice(1)
  }
}
