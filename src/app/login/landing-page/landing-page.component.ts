import {Component} from "@angular/core";
import {Router} from "@angular/router";
import { Trainer } from "src/app/models/trainer.model";
import { LoginService } from "src/app/services/login.service";
import { PokemonService } from "src/app/services/pokemon.service";


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
})
export class LandingPageComponent {

  public name: string = '';
  private trainer: Trainer[] = []
  private newTrainer: Trainer = {name: '', caughtPokemon: []};

  constructor(
    private readonly loginService: LoginService,
    private readonly pokemonService: PokemonService,
    private readonly router: Router) {
    if (loginService.alreadyLoggedIn()) {
      router.navigate(['pokemons']);
    }
  }

  public onSubmit(): void {
    this.newTrainer.name = this.name;
    localStorage.setItem('poke-trainer', JSON.stringify(this.newTrainer))
    this.loginService.logIn()
    this.router.navigate(['pokemons'])
  }

}
