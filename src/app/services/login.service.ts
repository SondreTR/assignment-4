import {Injectable} from "@angular/core";
// import {CaughtPokemonService} from "./caught-pokemon.service";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private loggedIn: boolean | undefined;

//   constructor(private readonly caughtPokemonService: CaughtPokemonService) {}


  public logIn(): void {
   this.loggedIn = true;
  }

  public alreadyLoggedIn(): boolean {
    if (!localStorage.getItem('poke-trainer')) {
      return false;
    }
    return true;
  }

  logOut() {
    localStorage.removeItem('poke-trainer')
    // this.caughtPokemonService.deleteCaughtPokemon();
    this.loggedIn = false;
  }
}
