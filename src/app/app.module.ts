import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PokemonPage } from './pokemons/pokemons.page';
import { AppRoutingModule } from './app-routing.module';
import { LoginPage } from './login/login.page';
import { LandingPageComponent } from './login/landing-page/landing-page.component';
import { PokemonCatalogueComponent } from './pokemon-catalogue/pokemon-catalogue.component';
import { PokemonCatalogueItemComponent } from './pokemon-catalogue-item/pokemon-catalogue-item.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonPage,
    LoginPage,
    LandingPageComponent,
    PokemonCatalogueComponent,
    PokemonCatalogueItemComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
