export interface Pokemon {
    id: number;
    name: string;
    types: any; 
    caught: boolean;
}

  
  