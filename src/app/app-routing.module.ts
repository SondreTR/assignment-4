import { NgModule } from "@angular/core";
import {RouterModule, Routes } from "@angular/router";
import { PokemonPage } from "./pokemons/pokemons.page";
import {LoginPage} from "./login/login.page";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'pokemons',
    component: PokemonPage
  },
  {
    path: 'login',
    component: LoginPage
  },
  ];

@NgModule(
  {
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  }
)
export class AppRoutingModule {}
