import {Component, OnInit} from "@angular/core";
import {PokemonService} from "../services/pokemon.service";
import {Router} from "@angular/router";
import { Pokemon } from "../models/pokemon.model";
import {LoginService} from "../services/login.service";

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
})
export class PokemonCatalogueComponent implements OnInit {
    constructor(
        private readonly pokemonService: PokemonService,
        private readonly loginService: LoginService,
        private router: Router
        ) {
            if(!this.loginService.alreadyLoggedIn()){
              this.router.navigate(['login'])
            }
  }

  ngOnInit(): void {
    this.pokemonService.fetchAllPokemon();
  }

  public get pokemons(): Pokemon[] {
    return this.pokemonService.pokemon();
  }

  public handlePokemonClicked(pokemon: Pokemon): void {
      pokemon.caught = true;
  }

  public goToTrainerPage(): void {
    this.router.navigate(['trainer'])
  }

  logOut() {
    this.loginService.logOut()
    this.router.navigate(['login'])
  }
}
